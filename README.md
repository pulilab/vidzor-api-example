Vidzor API example
====================

This is a simple project to showcase the Vidzor API.

The Vidzor API is a programmable interface to interact with embedded vidzor videos (aka vidzios)
 from your own webpage.

For an example, you might check out our own website http://vidzor.com that uses these APIs
to play vidzios integrated into the site.