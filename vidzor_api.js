(function() {
    'use strict';

    var window = this;
    var Vidzios = [];

    var VidzorAPI = window.VidzorAPI = function (iframeId) {
        this.iframe = document.getElementById(iframeId);
        Vidzios.push(this);
    }

    VidzorAPI.prototype.postMessage = function (data) {
        this.iframe.contentWindow.postMessage(data, this.iframe.src);
    };
    VidzorAPI.prototype.init = function () {
        this.postMessage({'message': 'init'});
    };
    VidzorAPI.prototype.play = function () {
        this.postMessage({'message': 'vidzioplay'});
    };
    VidzorAPI.prototype.replay = function () {
        this.postMessage({'message': 'vidzioreplay'});
    };
    VidzorAPI.prototype.loaded = function () {
        this.postMessage({'message': 'loaded'});
    };
    VidzorAPI.prototype.switchVideo = function (videoId) {
        this.postMessage({'message': 'switchVideo', 'video': videoId});
    };

    window.addEventListener('message', function () {
        switch (event.data.message) {
            // INIT #2: wait for the iframe to load and complete the handshake
            case "loaded":
                Vidzios.forEach(function (vidzio) {
                    vidzio.init();
                });
                break;
            case "vidzioloaded":
                break;
            case "vidzioended":
                break;
            default:
                console.info(event.data, "Message not implemented")
        }
    });
}).call(window);
